package menu

import (
	"fmt"
	"strconv"

	"github.com/bwmarrin/discordgo"
	"gitlab.com/winwizard/viscord/interface/messaging"
)

type MenuConfig struct {
	Session *discordgo.Session
}

type Menu struct {
	config         *MenuConfig
	currentChannel *discordgo.Channel
	currentGuild   string
	msgWindow      *messaging.MessageWindow
}

func NewMenu(config *MenuConfig) *Menu {
	return &Menu{
		config: config,
	}
}

func (m *Menu) GuildMenu() (*discordgo.Channel, error) {
	//TODO paginate guilds and handle people with way too many guilds (>100)
	guilds, err := m.config.Session.UserGuilds(100, "", "")
	if err != nil {
		return nil, fmt.Errorf("Could not fetch guilds. Error: %s", err)
	}

	//print guild menu
	fmt.Println("****GUILDS****")
	for id, guild := range guilds {
		fmt.Println("[", id, "] ", guild.Name)
	}
	for {
		var guildStr string
		fmt.Print("Choose a guild: ")
		fmt.Scanf("%s", &guildStr)
		switch guildStr {
		case "q":
		case "Q":
			//TODO make quit ask and have context
			return nil, nil
		default:
			if gid, err := strconv.ParseInt(guildStr, 10, 64); err == nil {
				if gid > int64(len(guilds)-1) {
					fmt.Println("Invalid Input [", guildStr, "], selected guild does not exist.")
					continue
				}
				m.currentGuild = guilds[gid].ID
				return m.ChannelMenu()

			} else {
				fmt.Println("Invalid Input [", guildStr, "], value must be a valid integer, or 'q' to quit.")
				continue
			}
		}
	}

}

func (m *Menu) ChannelMenu() (*discordgo.Channel, error) {
	fmt.Println("****Channels****")
	chans, err := m.config.Session.GuildChannels(m.currentGuild)
	if err != nil {
		return nil, fmt.Errorf("Could not fetch guild channels. Error: %s", err)
	}

	var textChans []*discordgo.Channel
	var textChanId int64
	for _, channel := range chans {
		if channel.Type != discordgo.ChannelTypeGuildVoice {
			//TODO support for voice channels would be sick
			textChans = append(textChans, channel)
			fmt.Println("[", textChanId, "] ", channel.Name)
			textChanId++
		}
	}
	for {
		var chanStr string
		fmt.Print("Choose a channel: ")
		fmt.Scanf("%s", &chanStr)
		switch chanStr {
		case "q":
		case "Q":
			//TODO make quit ask and have context
			return nil, nil
		default:
			if cid, err := strconv.ParseInt(chanStr, 10, 64); err == nil {
				if cid > int64(len(textChans)-1) {
					fmt.Println("Invalid Input [", chanStr, "], selected channel does not exist.")
					continue
				}
				m.currentChannel = textChans[cid]

				//FIXME Handle keeping program open alot better

				return m.currentChannel, nil

			} else {
				fmt.Println("Invalid Input [", chanStr, "], value must be a valid integer, or 'q' to quit.")
				continue
			}
		}
	}

	return nil, nil
}
