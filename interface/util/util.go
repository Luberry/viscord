package util

import (
	"strconv"

	x256 "github.com/ivolo/go-x256"
	"github.com/mgutz/ansi"
)

func RGBString(input string, rgbColor int) string {
	code := x256.ClosestCode(uint8((rgbColor>>16)&0xFF), uint8((rgbColor>>8)&0xFF), uint8(rgbColor&0xFF))
	return ansi.ColorCode(strconv.Itoa(code)) + input + ansi.ColorCode("reset")
}
