package messaging

import (
	"fmt"
	"strings"
	"sync"

	"github.com/bwmarrin/discordgo"
	"github.com/google/goterm/term"
	"github.com/jroimartin/gocui"
	log "github.com/sirupsen/logrus"
)

const (
	PM_GUILD_ID string = "xxxPMGUILDxxx"
)

type SidebarView struct {
	logger             *log.Entry
	session            *discordgo.Session
	guildExpand        map[string]bool
	currentChannel     string
	mutex              sync.Mutex
	handlerRemoveFuncs []func()
	update             func()
}

func NewSidebarView(logger *log.Entry, update func()) *SidebarView {
	sv := &SidebarView{
		logger:      logger.WithFields(log.Fields{"View": "SidebarView"}),
		guildExpand: make(map[string]bool),
		update:      update,
	}
	return sv
}

func (sbv *SidebarView) Update(view *gocui.View, session *discordgo.Session, guild string, channel string) {
	sbv.mutex.Lock()
	//TODO add gaps bet/n views
	view.Editable = false
	view.Highlight = true
	view.Frame = true
	view.Title = "Guilds"
	view.Wrap = true
	view.Autoscroll = true
	view.Clear()
	sbv.session = session
	sbv.currentChannel = channel
	sbv.guildExpand[guild] = true
	guildStr := sbv.formatGuildList()
	fmt.Fprintln(view, guildStr)
	sbv.mutex.Unlock()
}

func (sbv *SidebarView) formatGuildList() string {
	var b strings.Builder

	if pmString, err := sbv.formatPMS(); err != nil {
		sbv.logger.WithError(err).Error("Failed to fetch pms.")
	} else {
		fmt.Fprintln(&b, pmString)
	}

	//TODO paginate guilds and handle people with way too many guilds (>100)
	guilds, err := sbv.session.UserGuilds(100, "", "")
	if err != nil {
		sbv.logger.WithError(err).Error("Failed to fetch guilds.")
	} else {
		for _, guild := range guilds {
			fmt.Fprintln(&b, sbv.formatGuild(guild))
		}
	}
	return b.String()
}

func (sbv *SidebarView) formatPMS() (string, error) {
	var b strings.Builder
	fmt.Fprintln(&b, "Private Messages")
	if expand, ok := sbv.guildExpand[PM_GUILD_ID]; !ok {
		sbv.guildExpand[PM_GUILD_ID] = false
	} else if expand {
		var userChannels []*discordgo.Channel
		var err error
		if userChannels, err = sbv.session.UserChannels(); err != nil {
			return "", err
		}
		for _, channel := range userChannels {
			fmt.Fprintln(&b, sbv.formatChannel(channel))
		}
	}
	return b.String(), nil
}

func (sbv *SidebarView) formatGuild(guild *discordgo.UserGuild) string {
	var b strings.Builder
	fmt.Fprintln(&b, guild.Name)
	if expand, ok := sbv.guildExpand[guild.ID]; !ok {
		sbv.guildExpand[guild.ID] = false
	} else if expand {
		var realGuild *discordgo.Guild
		var err error
		if realGuild, err = sbv.session.Guild(guild.ID); err != nil {
			sbv.logger.WithError(err).WithFields(log.Fields{"GuildId": guild.ID, "GuildName": guild.Name}).Error("Failed to get info for guild.")
			return ""
		}

		for _, channel := range realGuild.Channels {
			fmt.Fprintln(&b, sbv.formatChannel(channel))
		}
	}
	return b.String()
}

func (sbv *SidebarView) formatChannel(channel *discordgo.Channel) string {
	name := channel.Name
	if channel.Type == discordgo.ChannelTypeGuildVoice {
		name = "(V) " + name
	}
	if channel.ID == sbv.currentChannel {
		return "\t" + term.BWhite(term.Black(term.Bold(term.Underline(name)))).String()
	}
	return "\t" + name
}

func (sbv *SidebarView) removeHandlers() {
	for _, fn := range sbv.handlerRemoveFuncs {
		fn()
	}
	sbv.handlerRemoveFuncs = []func(){}
}
